class Ok

  include ResultMethods

  def initialize(result)
    @result = result
  end

  def handle_result(success_handler, failure_handler)
    success_handler.call(@result)
  end

  def ==(other)
    other.map do |value|
      @result == value
    end.map_err do |err|
      false
    end.unwrap
  end
end
