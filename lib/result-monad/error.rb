class Error

  include ResultMethods

  def initialize(err)
    @err = err
  end

  def handle_result(success_handler, failure_handler)
    failure_handler.call(@err)
  end

  def ==(other)
    other.map do
      false
    end.map_err do |err|
      @err == err
    end.unwrap
  end
end
